﻿using IMDB.Data.Movies.Convert;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ItemModelType = System.Collections.Generic.Dictionary<string, object>;
namespace RecSys.Data.Converter
{
    class Program
    {
        public const string Feature_Class_Name = "class_interaction";

        static void Main(string[] args)
        {
            DateTime startTime = DateTime.Now;

            string trainFileName = args.Length > 0 && args[0] != null ? args[0] : @"D:\Programming\RecSys\RecSysChallenge2014\recsys_challenge_2014_dataset_public\training.dat";//@"D:\Programming\RecSys\RecSysChallenge2014\recsys_challenge_2014_dataset_public\training_0-500.dat";
            string testFileName = args.Length > 1 && args[1] != null ? args[1] : @"D:\Programming\RecSys\RecSysChallenge2014\recsys_challenge_2014_dataset_public\test.dat";//@"D:\Programming\RecSys\RecSysChallenge2014\recsys_challenge_2014_dataset_public\test.dat";
            string moviesFileName = args.Length > 2 && args[2] != null ? args[2] : null;// @"D:\Programming\RecSys\RecSysChallenge2014\recsys_challenge_2014_dataset_public\movies.dat";
            string filterPrefixes = args.Length > 3 && args[3] != null ? args[3] : "";
            
            var filterPrefixesList = filterPrefixes.Split(',', ';',' ');
            
            HashSet<string> commonFeatures = new HashSet<string>();

            //Dictionary<string, Tuple<decimal?, decimal?>> cmnFeatures = new Dictionary<string, Tuple<decimal?, decimal?>>();

            /***********************/
            /******Parse***********/
            /***********************/

            Dictionary<string, ItemModelType> movies = new Dictionary<string, ItemModelType>();
            //parse movie
            if (!string.IsNullOrEmpty(moviesFileName))
            {
                LoadMoviesData(moviesFileName, movies, commonFeatures);
            }

            //parse train data
            List<ItemModelType> trainInstances = new List<ItemModelType>();
            ParseInputData(trainFileName, trainInstances, commonFeatures, movies);

            //parse test data
            List<ItemModelType> testInstances = new List<ItemModelType>();
            if (!string.IsNullOrEmpty(testFileName))
            {
                ParseInputData(testFileName, testInstances, commonFeatures, movies);
            }

            /***********************/
            /******EXPORT***********/
            /***********************/

            //EXPORT TRAINING
            ExportFeaturesAndDataForLiblinear(trainFileName, commonFeatures, trainInstances, filterPrefixesList);

            //EXPORT TEST
            ExportFeaturesAndDataForLiblinear(testFileName, commonFeatures, testInstances, filterPrefixesList);

            /***********************/
            /******Statistics*******/
            /***********************/
            TimeSpan executionTime = DateTime.Now.Subtract(startTime);

            Console.WriteLine("Total instances count: {0}", trainInstances.Count + testInstances.Count);
            Console.WriteLine("Non zero interactions count: {0}", trainInstances.Count(i => int.Parse(i[Feature_Class_Name].ToString()) > 0));
            Console.WriteLine("Features count: {0}", commonFeatures.Count);
            Console.WriteLine("Total time: {0}", executionTime);

            //total ram memory consumption
            Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            long totalBytesOfMemoryUsed = currentProcess.WorkingSet64;
            Console.WriteLine("Total RAM: {0} MB", totalBytesOfMemoryUsed / (1024 * 1024));
        }

        private static void ExportFeaturesAndDataForLiblinear(string trainFileName, HashSet<string> commonFeatures, List<ItemModelType> trainInstances, string[] filterPrefixes)
        {
            //export features feature_name=value;
            HashSet<string> exportFeatures = new HashSet<string>();
            commonFeatures.Where(f => !f.StartsWith(Feature_Class_Name)
                    && !filterPrefixes.Contains(f, new StartsWithComparer())
                ).ToList()
                .ForEach((a) => { exportFeatures.Add(a); });

            SaveFeaturesFile(trainInstances, exportFeatures, trainFileName + ".features", Feature_Class_Name);

            //export for liblinear
            HashSet<string> libLinearFeatures = new HashSet<string>();
            commonFeatures.Where(f => !f.StartsWith("main_item_id") 
                            && !f.StartsWith("main_user_id") 
                            && !f.StartsWith("main_scraping_time") 
                            && !f.StartsWith(Feature_Class_Name)
                            && !filterPrefixes.Contains(f, new StartsWithComparer())
                            ).ToList()
                .ForEach((a) => { libLinearFeatures.Add(a); });

            SaveForLibLinear(trainInstances, libLinearFeatures, trainFileName + ".liblinear", Feature_Class_Name);
        }

        private static void SaveFeaturesFile(List<ItemModelType> instances, HashSet<string> features, string fileName, string classFeature)
        {
            if (features.Contains(classFeature))
            {
                throw new ArgumentException("Features array must not contain class features to insure that you exclude it from both train and test data!!!");
            }
            string[] featuresArray = features.OrderBy(f=>f).ToArray();


            using (TextWriter writer1 = new StreamWriter(fileName+".featurenames"))
            {
                for (int i = 0; i < featuresArray.Length; i++)
                {
                    writer1.WriteLine(featuresArray[i]+",");
                }
            }
            using (TextWriter writer = new StreamWriter(fileName))
            {
                foreach (var instance in instances)
                {
                    string classInteraction = instance[classFeature] != null ? instance[classFeature].ToString() : "-1";
                    writer.Write(classInteraction);
                    for (int i = 0; i < featuresArray.Length; i++)
                    {
                        object featureValue = null;
                        if (instance.TryGetValue(featuresArray[i], out featureValue))
                        {
                            writer.Write(" {0}={1}", featuresArray[i], featureValue);
                        }
                    }
                    writer.WriteLine();
                }
            }
        }

        private static void SaveForLibLinear(List<ItemModelType> instances, HashSet<string> features, string fileName, string classFeature)
        {
            if (features.Contains(classFeature))
            {
                throw new ArgumentException("Features array must not contain class features to insure that you exclude it from both train and test data!!!");
            }
            string[] featuresArray = features.ToArray();

            using (TextWriter writer = new StreamWriter(fileName))
            {
                foreach (var instance in instances)
                {
                    string classInteraction = instance[classFeature] != null ? instance[classFeature].ToString() : "-1";
                    writer.Write(classInteraction);
                    for (int i = 0; i < featuresArray.Length; i++)
                    {
                        object featureValue = null;
                        if (instance.TryGetValue(featuresArray[i], out featureValue))
                        {
                            writer.Write(" {0}:{1}", i + 1, featureValue);
                        }
                    }
                    writer.WriteLine();
                }
            }
        }

        public const string Const_TwitterDateTemplate = "ddd MMM dd HH:mm:ss +ffff yyyy";
        private static void ParseInputData(string recSysFileName, List<ItemModelType> instances, HashSet<string> commonFeatures, Dictionary<string, ItemModelType> movies)
        {
            string line = "";
            string delim = ",";

            using (TextReader reader = new StreamReader(recSysFileName))
            {
                string columnsName = reader.ReadLine();
                int recNumber = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    try
                    {

                        recNumber++;
                        int prevDelimIndex = 0;
                        int delimIndex = line.IndexOf(delim, 0);

                        ItemModelType instance_values = new ItemModelType();

                        //user_id,
                        string user_id = line.Substring(0, delimIndex);
                        prevDelimIndex = delimIndex;
                        AddFeature(instance_values, commonFeatures, user_id, "user_id", "main_");

                        //item_id,
                        delimIndex = line.IndexOf(delim, prevDelimIndex + 1);
                        string item_id = line.Substring(prevDelimIndex + 1, delimIndex - prevDelimIndex - 1).Trim();
                        AddFeature(instance_values, commonFeatures, int.Parse(item_id), "item_id", "main_");

                        prevDelimIndex = delimIndex;

                        //rating,
                        delimIndex = line.IndexOf(delim, prevDelimIndex + 1);
                        string rating = line.Substring(prevDelimIndex + 1, delimIndex - prevDelimIndex - 1).Trim();
                        int ratingValue = 5;
                        int.TryParse(rating, out ratingValue);
                        if (ratingValue >= 11)
                        {
                            //Console.WriteLine("Warning at line [{0}]:{1}", recNumber, "Rating is larger than 10 - " + ratingValue.ToString());
                            if (ratingValue <= 20)
                            {
                                ratingValue = 20;
                            }
                            else if (ratingValue<=100)
                            {
                                ratingValue = 100;
                            }
                            else 
                            {
                                ratingValue = 1000;
                            }
                        }

                        prevDelimIndex = delimIndex;
                        AddFeature(instance_values, commonFeatures, int.Parse(rating), "rating", "main_");

                        //scraping_time,
                        delimIndex = line.IndexOf(delim, prevDelimIndex + 1);
                        string scraping_time = line.Substring(prevDelimIndex + 1, delimIndex - prevDelimIndex - 1).Trim();
                        AddFeature(instance_values, commonFeatures, int.Parse(scraping_time), "scraping_time", "main_");

                        prevDelimIndex = delimIndex;

                        //parse tweet json
                        string tweet_json = line.Substring(prevDelimIndex + 1).Trim();
                        JObject tweetObject = JObject.Parse(tweet_json);
                        JsonToDictionary((JToken)tweetObject, instance_values, commonFeatures, "tw_");

                        //add movies
                        string movieId = item_id;
                        if (movies.ContainsKey(item_id))
                        {
                            var movieFeatures = movies[item_id];
                            foreach (var movieFeature in movieFeatures)
                            {
                                instance_values.Add(movieFeature.Key, movieFeature.Value);
                            }
                        }

                        //class - interaction        
                        int interaction = int.Parse(instance_values["tw_favorite_count"].ToString()) + int.Parse(instance_values["tw_retweet_count"].ToString());
                        instance_values.Add(Feature_Class_Name, interaction);


                        instances.Add(instance_values);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error at line [{0}]:{1} in {2}", recNumber, e.ToString(), recSysFileName);
                    }
                }
            }
        }

        private static void LoadMoviesData(string moviesFileName, Dictionary<string, ItemModelType> movies, HashSet<string> commonFeatures)
        {
            string line = "";
            string delim = ",";

            using (TextReader reader = new StreamReader(moviesFileName))
            {
                int recNumber = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    try
                    {
                        recNumber++;
                        int prevDelimIndex = 0;
                        int delimIndex = line.IndexOf(delim, 0);

                        //movie
                        string movie_id = line.Substring(0, delimIndex);
                        prevDelimIndex = delimIndex;
                        //AddFeature(instance_values, commonFeatures, movie_id, "movie_id", "imdb_id_");


                        //parse tweet json
                        string movie_json = line.Substring(prevDelimIndex + 1).Trim();
                        var movie = JsonConvert.DeserializeObject<IMDBMovie>(movie_json);
                        var instance_values = movie.ToDictionary(false, false, false);


                        foreach (var instanceValue in instance_values)
                        {
                            if (!commonFeatures.Contains(instanceValue.Key))
                            {
                                commonFeatures.Add(instanceValue.Key);
                            }
                        }

                        if (!movies.ContainsKey(movie_id))
                        {
                            movies.Add(movie_id, instance_values);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error at line [{0}]:{1} in {2}", recNumber, e.ToString(), moviesFileName);
                    }
                }
            }
        }

        private static DateTime DateTime_Scale_Substract_Date = new DateTime(2012, 12, 01);
        public static void JsonToDictionary(JToken jsonObject, ItemModelType dict, HashSet<string> common_features, string featurePrefix)
        {
            foreach (var item in jsonObject)
            {
                switch (item.Type)
                {
                    case JTokenType.Array:
                        break;
                    case JTokenType.Bytes:
                        break;
                    case JTokenType.Comment:
                        break;
                    case JTokenType.Constructor:
                        break;
                    case JTokenType.Boolean:
                        AddFeature(dict, common_features, (decimal)(((bool)((JValue)item).Value) ? 1 : 0), item.Path, featurePrefix);
                        break;
                    case JTokenType.Date:
                        //AddFeature(dict, common_features, (object)(((DateTime)((JValue)item).Value).Subtract(new DateTime(1.1.2013))), item.Path, featurePrefix);
                        decimal featValue = (decimal)(((DateTime)((JValue)item).Value).Subtract(DateTime_Scale_Substract_Date).TotalDays);
                        AddFeature(dict, common_features, featValue, item.Path, featurePrefix);
                        break;
                    case JTokenType.Float:
                        AddFeature(dict, common_features, (decimal)(((JValue)item).Value), item.Path, featurePrefix);
                        break;
                    case JTokenType.Guid:
                        AddFeature(dict, common_features, 0, item.Path, featurePrefix);
                        break;
                    case JTokenType.Integer:
                        AddFeature(dict, common_features, ((JValue)item).Value, item.Path, featurePrefix);
                        break;
                    case JTokenType.String:
                        AddFeature(dict, common_features, 1, item.Path, featurePrefix);
                        break;
                    case JTokenType.TimeSpan:
                        AddFeature(dict, common_features, (decimal)(((TimeSpan)((JValue)item).Value).TotalDays), item.Path, featurePrefix);
                        break;
                    case JTokenType.Uri:
                        AddFeature(dict, common_features, 1, item.Path, featurePrefix);
                        break;
                    case JTokenType.None:
                        break;
                    case JTokenType.Null:
                        AddFeature(dict, common_features, 0, item.Path, featurePrefix);
                        break;
                    case JTokenType.Object:
                    case JTokenType.Property:
                        JsonToDictionary(item, dict, common_features, featurePrefix);
                        break;
                    case JTokenType.Raw:
                        break;
                    default:
                        break;
                }
            }
        }

        private static void AddFeature(ItemModelType dict, HashSet<string> common_features, object itemValue, string featureName, string featurePrefix)
        {
            string feature = featurePrefix + featureName;
            dict.Add(feature, itemValue);
            if (!common_features.Contains(feature))
            {
                common_features.Add(feature);
            }
        }


    }

    public class StartsWithComparer : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            if ((string.IsNullOrEmpty(x)) && (!string.IsNullOrEmpty(y)))
            {
                return false;
            }
            return (y.StartsWith(x, StringComparison.InvariantCultureIgnoreCase));
        }

        public int GetHashCode(string obj)
        {
            throw new NotImplementedException();
        }
    }
}
