﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IMDB.Data.Movies.Convert
{
    public class IMDBMovie
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Rated { get; set; }
        public string Released { get; set; }
        public string Runtime { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Writer { get; set; }
        public string Actors { get; set; }
        public string Plot { get; set; }
        public string Language { get; set; }
        public string Country { get; set; }
        public string Awards { get; set; }
        public string Poster { get; set; }
        public string Metascore { get; set; }
        public string imdbRating { get; set; }
        public string imdbVotes { get; set; }
        public string imdbID { get; set; }
        public string Type { get; set; }
        public string Response { get; set; }

        public Dictionary<string, object> ToDictionary(bool includeActors = true, bool includeWriters = true, bool includeDirectors = true)
        {
            try
            {
                var features = new Dictionary<string, object>();
                //AddFeatureIfNotExists(features,  string.Empty, (object)0);

                int movieYear = 0;
                var yearMatch = Regex.Match(Year, "[0-9]+");
                int.TryParse(yearMatch.Captures.Count > 0 ? yearMatch.Captures[0].Value : "0", out movieYear);

                AddFeatureIfNotExists(features, "imdb_Year", movieYear);
                AddFeatureIfNotExists(features, "imdb_Rated_" + ToFeatureName(this.Rated), 1);
                //features.Add("imdb_Released", int.Parse(this.Released));

                //runtime
                var runtime = Regex.Matches(Runtime, "[0-9]+");
                AddFeatureIfNotExists(features, "imdb_Runtime", runtime.Count > 0 ? int.Parse(runtime[0].Value) : 0);

                //Genres
                var genres = this.Genre.Split(',');
                foreach (var genre in genres)
                {
                    AddFeatureIfNotExists(features, "imdb_genre_" + ToFeatureName(genre), 1);
                }

                //directors
                if (includeDirectors)
                {
                    var directors = this.Director.Split(',');
                    foreach (var director in directors)
                    {
                        AddFeatureIfNotExists(features, "imdb_d_" + ToFeatureName(director), 1);
                    }
                }

                //writers
                if (includeWriters)
                {
                    var writers = this.Writer.Split(',');
                    foreach (var writer in writers)
                    {
                        int bracketIndex = writer.IndexOf('(');
                        string writer_name = bracketIndex > 0 ? writer.Substring(0, bracketIndex - 1) : writer;
                        AddFeatureIfNotExists(features, "imdb_w_" + ToFeatureName(writer_name), 1);
                    }
                }

                //actors
                if (includeActors)
                {
                    var actors = this.Actors.Split(',');
                    foreach (var actor in actors)
                    {
                        int bracketIndex = actor.IndexOf('(');
                        string actor_name = bracketIndex > 0 ? actor.Substring(0, bracketIndex - 1) : actor;
                        AddFeatureIfNotExists(features, "imdb_act_" + ToFeatureName(actor_name), 1);
                    }
                }

                //languages
                var languages = this.Language.Split(',');
                foreach (var lang in languages)
                {
                    AddFeatureIfNotExists(features, "imdb_lang_" + ToFeatureName(lang.ToLower()), 1);
                }

                //features.Add("imdb_Country", int.Parse(this.Country));
                var countries = this.Country.Split(',');
                foreach (var country in countries)
                {
                    AddFeatureIfNotExists(features, "imdb_country_" + ToFeatureName(country.ToLower()), 1);
                }

                //0064665,{"Title":"Midnight Cowboy","Year":"1969","Rated":"X","Released":"16 Jun 1969","Runtime":"113 min","Genre":"Drama","Director":"John Schlesinger","Writer":"Waldo Salt (screenplay), James Leo Herlihy (based on the novel by)","Actors":"Dustin Hoffman, Jon Voight, Sylvia Miles, John McGiver","Plot":"A naive male prostitute and his sickly friend struggle to survive on the streets of New York City.","Language":"English, Italian","Country":"USA","Awards":"Won 3 Oscars. Another 26 wins & 12 nominations.","Poster":"http://ia.media-imdb.com/images/M/MV5BMTMzMjY1OTE5OV5BMl5BanBnXkFtZTcwMjUzNTk3NA@@._V1_SX300.jpg","Metascore":"N/A","imdbRating":"8.0","imdbVotes":"62,713","imdbID":"tt0064665","Type":"movie","Response":"True"}
                //var awardMatches = Regex.Matches(Awards,@"([0-9]+)\swin");    
                Dictionary<string, object> awardFeatures = new Dictionary<string, object>();
                if (TryParseAwards(Awards, awardFeatures))
                {
                    foreach (var awardFeature in awardFeatures)
                    {
                        AddFeatureIfNotExists(awardFeatures, awardFeature.Key, awardFeature.Value);
                    }
                }
                //features.Add("imdb_Awards", int.Parse(this.Awards));

                //features.Add("imdb_Poster", 1);
                decimal metascore = 0;
                Decimal.TryParse(this.Metascore, out metascore);
                AddFeatureIfNotExists(features, "imdb_Metascore", metascore);

                //rating
                decimal rating = 0;
                decimal.TryParse(imdbRating, out rating);
                AddFeatureIfNotExists(features, "imdb_imdbRating", rating);

                //votes
                int votes = 0;
                int.TryParse(imdbVotes.Replace(",", ""), out votes);
                AddFeatureIfNotExists(features, "imdb_imdbVotes", votes);
                // AddFeatureIfNotExists(features, "imdb_imdbID_" + this.imdbID, 1);
                AddFeatureIfNotExists(features, "imdb_Type_" + ToFeatureName(Type), 1);
                AddFeatureIfNotExists(features, "imdb_Response", bool.Parse(this.Response)?1:0);

                return features;

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void AddFeatureIfNotExists(Dictionary<string, object> features, string key, object value)
        {
            if (!features.ContainsKey(key))
            {
                features.Add(key, value);
            }
        }

        public static string ToFeatureName(string val)
        {
            string featureName = Regex.Replace(val.Trim(), @"[\\\/\,\(\)\.\s]+", "_", RegexOptions.IgnoreCase)
                                    .ToLower()
                                    .Replace(" ", "_");
            return featureName;
        }

        public static bool TryParseAwards(string awards, Dictionary<string, object> features)
        {
            try
            {
                var awardMatches = Regex.Matches(awards, @"(?<a_num>[0-9]+)\s(?<a_name>[a-zA-Z]+)");
                for (int i = 0; i < awardMatches.Count; i++)
                {
                    string name = string.Empty;
                    string num = string.Empty;

                    if (awardMatches[i].Groups["a_num"].Success)
                    {
                        name = awardMatches[i].Groups["a_name"].Success ? awardMatches[i].Groups["a_name"].Value : "";
                        num = awardMatches[i].Groups["a_num"].Success ? awardMatches[i].Groups["a_num"].Value : "";
                    }

                    if (!string.IsNullOrEmpty(name))
                    {
                        features.Add(string.Format("imdb_award_" + name.ToLower()), num);
                    }

                }
            }
            catch (Exception)
            {
                Console.WriteLine("AWARDS - Error parsing awards - \"{0}\"", awards);
                return false;
            }

            return true;
        }
    }

}
