﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using IMDB.Data.Movies.Convert;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace RecSys.Data_Tests
{
    [TestClass]
    public class IMDB_Parse_Tests
    {
        [TestMethod]
        public void Test_Moview_ToDictionary()
        {
            string movie_json = "{\"Title\":\"Midnight Cowboy\",\"Year\":\"1969\",\"Rated\":\"X\",\"Released\":\"16 Jun 1969\",\"Runtime\":\"113 min\",\"Genre\":\"Drama\",\"Director\":\"John Schlesinger\",\"Writer\":\"Waldo Salt (screenplay), James Leo Herlihy (based on the novel by)\",\"Actors\":\"Dustin Hoffman, Jon Voight, Sylvia Miles, John McGiver\",\"Plot\":\"A naive male prostitute and his sickly friend struggle to survive on the streets of New York City.\",\"Language\":\"English, Italian\",\"Country\":\"USA\",\"Awards\":\"Won 3 Oscars. Another 26 wins & 12 nominations.\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTMzMjY1OTE5OV5BMl5BanBnXkFtZTcwMjUzNTk3NA@@._V1_SX300.jpg\",\"Metascore\":\"N/A\",\"imdbRating\":\"8.0\",\"imdbVotes\":\"62,713\",\"imdbID\":\"tt0064665\",\"Type\":\"movie\",\"Response\":\"True\"}";
            var movie = JsonConvert.DeserializeObject<IMDBMovie>(movie_json);
            var features = movie.ToDictionary();

            Assert.AreNotEqual(0, features.Count);
        }

        [TestMethod]
        public void Test_Movie_ParseAwards()
        {
            string awards = "Won 3 Oscars. Another 26 wins & 12 nominations.";
            Dictionary<string, object> features = ParseAwards(awards);

            Assert.AreNotEqual(features.Count, 0);
        }

        private static Dictionary<string, object> ParseAwards(string awards)
        {
            Dictionary<string, object> features = new Dictionary<string, object>();

            try
            {
                var awardMatches = Regex.Matches(awards, @"(?<a_num>[0-9]+)\s(?<a_name>[a-zA-Z]+)");
                for (int i = 0; i < awardMatches.Count; i++)
                {
                    string name = string.Empty;
                    string num = string.Empty;

                    if (awardMatches[i].Groups["a_num"].Success)
                    {
                        name = awardMatches[i].Groups["a_name"].Success ? awardMatches[i].Groups["a_name"].Value : "";
                        num = awardMatches[i].Groups["a_num"].Success ? awardMatches[i].Groups["a_num"].Value : "";
                    }

                    if (!string.IsNullOrEmpty(name))
                    {
                        features.Add(string.Format("imdb_award_" + name.ToLower()), num);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return features;
        }
    }
}
