SET LIBSVM_PATH=E:\Software\libsvm-3.18\windows
SET LIBLINEAR_PATH=E:\Software\liblinear-1.94\windows
SET TRAIN_FILE=training.dat.liblinear 
SET TEST_FILE=test.dat.liblinear 

# Prediction
%LIBLINEAR_PATH%\train.exe -s 6 -c 1 training.dat.liblinear training.dat.liblinear.model
%LIBLINEAR_PATH%\predict.exe test.dat.liblinear training.dat.liblinear.model test.dat.liblinear.predictions
