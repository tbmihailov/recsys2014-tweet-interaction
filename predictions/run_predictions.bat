SET LIBSVM_PATH=E:\Software\libsvm-3.18\windows
SET LIBLINEAR_PATH=E:\Software\liblinear-1.94\windows
SET TRAIN_FILE=training.dat.liblinear 
SET TEST_FILE=test.dat.liblinear 

# Cross_validation:
%LIBLINEAR_PATH%\train.exe -c 1 -v 2 %TRAIN_FILE%


# Prediction
%LIBLINEAR_PATH%\train.exe -c 1 %TRAIN_FILE% %TRAIN_FILE%.model
%LIBLINEAR_PATH%\predict.exe %TEST_FILE% %TRAIN_FILE%.model %TEST_FILE%.predictions


#############
## Scaling ##
#############

%LIBSVM_PATH%\svm-scale -l 0 -s train+dev2013-B-GATE.txt.range %TRAIN_FILE% > %TRAIN_FILE%.scale
%LIBSVM_PATH%\svm-scale -r train+dev2013-B-GATE.txt.range %TEST_FILE% > %TEST_FILE%.scale

# Scaled Cross_validation:
%LIBLINEAR_PATH%\train.exe -c 0.02 -v 2 %TRAIN_FILE%.scale
# Cross Validation Accuracy = 70.919%


# Scaled Prediction:

%LIBLINEAR_PATH%\train.exe -c 0.012 %TRAIN_FILE%.scale %TRAIN_FILE%.scale.model
%LIBLINEAR_PATH%\predict.exe %TEST_FILE%.scale %TRAIN_FILE%.scale.model %TEST_FILE%.scale.predictions
	# Accuracy = 70.764% (2510/3547)

#Жоро: Stoch MaxEnt  Test  Accuracy = 0.6974908373273189


perl predictions-to-text.pl test2014-GATE.txt.liblinear.scale.predictions